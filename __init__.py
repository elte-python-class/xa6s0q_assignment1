
# coding: utf-8

# In[515]:


class Protein:

        def __init__(self, db, id, name, os, gn, ox, pe, sv):

                self.db = db
                self.id = id
                self.name = name
                self.os = os
                self.gn = gn
                self.ox = int(ox)
                self.pe = int(pe)
                self.sv = int(sv)
                
                self.s = ""
                self.size = 0
                
                self.aminoacidcounts = {
                    "A" : 0,
                    "C" : 0,
                    "D" : 0,
                    "E" : 0,
                    "F" : 0,
                    "G" : 0,
                    "H" : 0,
                    "I" : 0,
                    "K" : 0,
                    "L" : 0,
                    "M" : 0,
                    "N" : 0,
                    "O" : 0,
                    "P" : 0,
                    "Q" : 0,
                    "R" : 0,
                    "S" : 0,
                    "T" : 0,
                    "U" : 0,
                    "V" : 0,
                    "W" : 0,
                    "Y" : 0
                }

        def __repr__(self):
                return self.name+ ' id: '+ self.id

        def __eq__(self, other):
            return self.size == other.size

        def __ne__(self, other):
            return self.size != other.size

        def __lt__(self, other):
            return self.size < other.size

        def __le__(self, other):
            return self.size <= other.size

        def __gt__(self, other):
            return self.size > other.size

        def __ge__(self, other):
            return self.size >= other.size



# In[562]:


def sort_proteins_pe(proteins):
    def for_sort_pe(proteins):
        return proteins.pe
    sorted_proteins = sorted(proteins, key = for_sort_pe, reverse = True)
    return sorted_proteins


# In[558]:


def sort_proteins_aa(proteins, single_letter):
    sorted_proteins = sorted(proteins, key = lambda p: p.s.count(single_letter), reverse = True)
    return sorted_proteins


# In[569]:


def find_protein_with_motif(proteins, motif):
    found_proteins = [p for i, p in enumerate(proteins) if p.s.count(motif) > 0] 
    return found_proteins


# In[582]:


def load_fasta(fasta):
        
        #opening and closing the file, reading the text
        opened_file = open(fasta, "r")
        raw_read = opened_file.read()
        opened_file.close()
        
        lines = list(raw_read.split("\n")) 
        
        
        headers = [l for l in lines if l.startswith(">")]
        indexes = [i for i, l in enumerate(lines) if l.startswith(">")]
        
        raw_seqs = []
        for i in range(len(indexes)):
            if i < (len(indexes)-1):
                raw_seqs.append(lines[indexes[i]:indexes[i+1]])
            else:
                raw_seqs.append(lines[indexes[i]:])
        
        just_seqs = []
        for s in raw_seqs:
            just_seqs.append(s[1:])
            
        seqs_joined = []
        for j in just_seqs:
            seqs_joined.append("".join(j))
     
        proteins = []
        for h in headers:
            proteins.append(Protein(db = str(h).split(">")[1].split("|")[0],
                                  id = str(h).split("|")[1].split("|")[0],
                                  name = (str(h)[::-1].split("=SO ")[1].split("|")[0])[::-1],
                                  os = str(h).split("OS=")[1].split(" OX=")[0],
                                  gn = str(h).split("GN=")[1].split(" PE=")[0],
                                  ox = str(h).split("OX=")[1].split(" GN=")[0],
                                  pe = str(h).split("PE=")[1].split(" SV=")[0],
                                  sv = str(h).split("SV=")[1].split("'")[0]))
        
        for p in range(len(proteins)):
            proteins[p].s = str(seqs_joined[p])
            proteins[p].size = len(seqs_joined[p])
            
            proteins[p].aminoacidcounts["A"] = seqs_joined[p].count("A")
            proteins[p].aminoacidcounts["C"] = seqs_joined[p].count("C")
            proteins[p].aminoacidcounts["D"] = seqs_joined[p].count("D")
            proteins[p].aminoacidcounts["E"] = seqs_joined[p].count("E")
            proteins[p].aminoacidcounts["F"] = seqs_joined[p].count("F")
            proteins[p].aminoacidcounts["G"] = seqs_joined[p].count("G")
            proteins[p].aminoacidcounts["H"] = seqs_joined[p].count("H")
            proteins[p].aminoacidcounts["I"] = seqs_joined[p].count("I")
            proteins[p].aminoacidcounts["K"] = seqs_joined[p].count("K")
            proteins[p].aminoacidcounts["L"] = seqs_joined[p].count("L")
            proteins[p].aminoacidcounts["M"] = seqs_joined[p].count("M")
            proteins[p].aminoacidcounts["N"] = seqs_joined[p].count("N")
            proteins[p].aminoacidcounts["O"] = seqs_joined[p].count("O")
            proteins[p].aminoacidcounts["P"] = seqs_joined[p].count("P")
            proteins[p].aminoacidcounts["Q"] = seqs_joined[p].count("Q")
            proteins[p].aminoacidcounts["R"] = seqs_joined[p].count("R")
            proteins[p].aminoacidcounts["S"] = seqs_joined[p].count("S")
            proteins[p].aminoacidcounts["T"] = seqs_joined[p].count("T")
            proteins[p].aminoacidcounts["U"] = seqs_joined[p].count("U")
            proteins[p].aminoacidcounts["V"] = seqs_joined[p].count("V")
            proteins[p].aminoacidcounts["W"] = seqs_joined[p].count("W")
            proteins[p].aminoacidcounts["Y"] = seqs_joined[p].count("Y")
        
        #returning the list
        return proteins
